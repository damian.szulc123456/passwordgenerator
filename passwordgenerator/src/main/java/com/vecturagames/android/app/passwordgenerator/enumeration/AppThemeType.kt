package com.vecturagames.android.app.passwordgenerator.enumeration

enum class AppThemeType {

    LIGHT, DARK

}
